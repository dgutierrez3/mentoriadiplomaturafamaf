# Predicciones de Picking de Producto

# Objetivo

El proyecto consiste en predecir los productos a despachar para realizar el picking de productos antes que se concrete la venta de los mismos y asi optimizar los tiempos, costos de entrega y evitar tiempos muertos en depostio.  Se trata de comenzar el proceso de logística aun antes que los pedidos sean efectivamente realizados por los clientes.

# Descripcion del dataset

El dataset contiene información de productos vendidos del sistema de ventas con una muestra de dias, semanas y meses que permita detectar los comportamientos temporales de ventas de productos y asi poder anticipar el picking de productos a despachar antes de recibir la totalidad de los pedidos. 

- productos.csv: información de los productos vendidos. Contiene:
	sku: identificador de producto
	descripcion: descripcion del producto enmascarado
	marca: marca del producto
	id categoria: identificador de categoria a la cual pertenece el producto
	presentacion: forma de presentacion de producto (pack, kilogramos, etc)
	unidadcm3: cm3 del producto
	unidadkg: kg del producto
	id proveedor: identificador del proveedor del producto

- categoria.csv: información de categoria de productos vendidos: Contiene
	id categoria: identificador de categoria.
	nombre: nombre de la categoria

- puntos de venta.csv: información de puntos de ventas. Contiene:
	id_punto_venta: identificador de punto de venta
	nombre: nombre de punto de venta enmascarado
	id_provincia: identificador de la provincia a la que pertenece el punto de venta
	id_localidad: identificador de la localidad a la que pertenece el punto de venta
	id_pais: identificador del pais al que pertenece el punto de venta

- paises.csv: información de paises. Contiene:
	id pais: identificador de país
	nombre: nombre de país

- provincias.csv: información de provincias. Contiene:
	id provincia: identificador de provincia
	nombre: nombre de provincia

- localidades.csv: información de localidades. Contiene:
	id localidad: identificador de localidad
	nombre: nombre de la localidad

- ventas.csv: información de ventas. Contiene:
	dia: dia de la venta
	mes: mes de la venta
	año: año de la venta
	hora: hora en que se produzco la venta
	sku: identificador de producto
	cantidad pedida: cantidad comprada por punto de venta
	id_punto_venta: identificador de punto de venta
	

