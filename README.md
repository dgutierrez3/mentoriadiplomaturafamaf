# Predicciones de Demanda de Producto

# Descripción 

En la era de la transformación digital las organizaciones de todos los sectores de la sociedad se han visto en la necesidad de integrar nuevas tecnologías en todas las áreas de sus organizaciones para optimizar sus procesos, aumentar su calidad, mejorar su competitividad, ofrecer valor, incrementar la satisfacción de sus clientes y brindar nuevas experiencias de compra. Uno de los factores mas importantes en la experiencia positiva del consumidor es la inmediatez en la entrega del producto comprado. Es aquí es donde, en la actualidad, existen compañías disruptivas que trabajan con ciencia de datos para lograr adelantarse a la distribución del producto incluso antes de ser comprado prediciendo la demanda con cierto grado de certidumbre. Esto impone un cambio de paradigma que apunta a un modelo de producción “Inteligente” que permita adelantarse a futuras ventas y cubrir las necesidades de stock reduciendo el tiempo de entrega.

El conjunto de datos que utilizaremos contiene productos vendidos de los últimos 5 años de una compañía de venta de alimentos congelados en distintos países de la región. El objetivo final de la mentoría es poder predecir el próximo mes de venta de productos en los diferentes países y zonas en donde opera esta compañía. Sin embargo, los prácticos que se realizarán en esta mentoría son aplicables a compañías de cualquier vertical de negocio que quiera predecir la demanda.

Vamos a utilizar lo aprendido en la diplomatura para atravesar todas las fases de un proyecto de machine learning.  En este sentido vamos a establecer el marco de un problema de predicción de demanda, entender los datos y sus comportamientos temporales, realizar ingeniería de características, incorporar nuevas variables obteniendo de internet información que puede enriquecer el conjunto de datos original, aplicar modelos de machine learning para finalmente presentar los resultados obtenidos. 

# Objetivo

El proyecto consiste en predecir los productos a despachar para realizar el picking de productos antes que se concrete la venta de los mismos y asi optimizar los tiempos, costos de entrega y evitar tiempos muertos en depostio.  Se trata de comenzar el proceso de logística aun antes que los pedidos sean efectivamente realizados por los clientes.

# Descripcion del dataset

El dataset contiene información de productos vendidos del sistema de ventas con una muestra de dias, semanas y meses que permita detectar los comportamientos temporales de ventas de productos y asi poder anticipar el picking de productos a despachar antes de recibir la totalidad de los pedidos. 

- productos.csv: información de los productos vendidos. Contiene:
	sku: identificador de producto
	descripcion: descripcion del producto enmascarado
	marca: marca del producto
	id categoria: identificador de categoria a la cual pertenece el producto
	presentacion: forma de presentacion de producto (pack, kilogramos, etc)
	unidadcm3: cm3 del producto
	unidadkg: kg del producto
	id proveedor: identificador del proveedor del producto

- categoria.csv: información de categoria de productos vendidos: Contiene
	id categoria: identificador de categoria.
	nombre: nombre de la categoria

- puntos de venta.csv: información de puntos de ventas. Contiene:
	id_punto_venta: identificador de punto de venta
	nombre: nombre de punto de venta enmascarado
	id_provincia: identificador de la provincia a la que pertenece el punto de venta
	id_localidad: identificador de la localidad a la que pertenece el punto de venta
	id_pais: identificador del pais al que pertenece el punto de venta

- paises.csv: información de paises. Contiene:
	id pais: identificador de país
	nombre: nombre de país

- provincias.csv: información de provincias. Contiene:
	id provincia: identificador de provincia
	nombre: nombre de provincia

- localidades.csv: información de localidades. Contiene:
	id localidad: identificador de localidad
	nombre: nombre de la localidad

- ventas.csv: información de ventas. Contiene:
	dia: dia de la venta
	mes: mes de la venta
	año: año de la venta
	hora: hora en que se produzco la venta
	sku: identificador de producto
	cantidad pedida: cantidad comprada por punto de venta
	id_punto_venta: identificador de punto de venta
	

